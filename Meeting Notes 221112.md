Assignment needs to be done in R. It is a good idea to already look at some other / existing solutions, although most are in python.The deadline is relatively near :-). 
"Distance" is a challenge: manhattan distance can be used but is not accurate if you are near the park or cross the river.Haversign distances are sometimes used.Mapbox has an API to calculate exact distance. However, the algorithm needs to be self-contained. Maybe we can define sectors to 
How do we share code: gitlab. Julien will set up a repository.Model selection: take some and look at the initial result => select the one with the best results.
Add data / columns: day of the week; holiday; maybe time bucketsWhat about weather conditions? The dataset will be on the same range of dates => if we find a source we could include it. To be checked if that is possible / allowed.
high level planning:
  week 1: explore data and test and test initial approaches. We meet Sunday 20th at 10am.    
    Johan: linear regression and look into distance function; 
    Julien: random forest; 
    Therese: KNN; 
    Eddy: PCA
  week 2: further refine / enhance approach to increase the score. Run the script on the supercomputer (?): Sunday 27th at 10am.
  week 3: write the report (in Rmd). Friday 2nd of December at 8am. Saturday 3rd / Sunday 4th: TBD.Therese will send the calendar / google meet invites.
